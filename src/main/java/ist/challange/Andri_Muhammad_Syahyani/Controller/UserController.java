package ist.challange.Andri_Muhammad_Syahyani.Controller;

import ist.challange.Andri_Muhammad_Syahyani.DTO.RegistrationDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

public class UserController {
    private final UserService userService;
    @PostMapping("/registration")
    public ResponseEntity<Object> registrationUser(@RequestBody RegistrationDTO registrationDTO) throws Exception {
        return userService.registrationUsers(registrationDTO);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> authenticateUser(@Valid @RequestBody RegistrationDTO registrationDTO) throws Exception {
        return userService.loginUsers(registrationDTO);
    }

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers() throws Exception {
        return userService.findAllUsers();
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<Object> editUsers(@PathVariable long id, @RequestBody RegistrationDTO registrationDTO) throws Exception {
        return userService.editUser(registrationDTO, id);
    }
}
