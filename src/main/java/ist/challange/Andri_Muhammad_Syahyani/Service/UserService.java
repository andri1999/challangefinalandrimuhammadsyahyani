package ist.challange.Andri_Muhammad_Syahyani.Service;

import ist.challange.Andri_Muhammad_Syahyani.DTO.RegistrationDTO;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<Object> registrationUsers(RegistrationDTO registrationDTO) throws Exception;
    ResponseEntity<Object> loginUsers(RegistrationDTO registrationDTO) throws Exception;
    ResponseEntity<Object> findAllUsers()throws Exception;
    ResponseEntity<Object> editUser(RegistrationDTO registrationDTO,long id)throws Exception;
}
