package ist.challange.Andri_Muhammad_Syahyani.DTO;

import ist.challange.Andri_Muhammad_Syahyani.Entity.Users;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegistrationDTO {
    private String username;
    private String password;

    public Users saveToEntity() {
        return Users.builder()
                .username(this.username)
                .password(this.password)
                .build();
    }

}