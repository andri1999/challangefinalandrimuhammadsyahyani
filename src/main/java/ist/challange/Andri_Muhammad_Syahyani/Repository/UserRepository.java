package ist.challange.Andri_Muhammad_Syahyani.Repository;

import ist.challange.Andri_Muhammad_Syahyani.Entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository  extends JpaRepository<Users, Long> {
    Optional<Users> findByUsername(String keyword);

}